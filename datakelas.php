<?php

include_once("koneksi.php");

$nama_kelas     ="";
$prodi          ="";
$fakultas       ="";
$suksess        ="";
$error          ="";

if(isset($_GET['op'])){
    $op = $_GET['op'];
}else{
    $op ="";
}
if($op == 'delete'){
    $id         = $_GET['id_kelas'];
    $sql1       = "DELETE FROM kelas WHERE id_kelas = '$id'";
    $konek1     = mysqli_query($connect,$sql1);
    if($konek1){
        $suksess = "Berhasil hapus data";
    }else{
        $error  = "Gagal melakukan delete data";
    }
}
if($op == 'edit'){
    $id             = $_GET['id_kelas'];
    $sql1           = "SELECT * FROM kelas WHERE id_kelas = '$id'";
    $konek1         = mysqli_query($connect,$sql1);
    $r1             = mysqli_fetch_array($konek1);
    $nama_kelas     = $r1['nama_kelas'];
    $prodi          = $r1['prodi'];
    $fakultas       = $r1['fakultas'];
    
    if($nama_kelas == ''){
        $error  = "Data tidak ada";
    }
}
if(isset($_POST['simpan'])){ // untuk create data
    $nama_kelas     =$_POST['nama_kelas'];
    $prodi          =$_POST['prodi'];
    $fakultas       =$_POST['fakultas'];

    if($nama_kelas && $prodi && $fakultas){
        if($op == 'edit'){ // untuk update
            $sql1       = "update kelas set nama_kelas = '$nama_kelas',prodi = '$prodi',fakultas = '$fakultas' where id_kelas = '$id'";
            $konek1     = mysqli_query($connect,$sql1);
            if($konek1){
                $suksess = "Data berhasil di Update";
            }else{
                $error   = "Data gagal di Update";
            }
        }else{ // untuk insert
            $sql1        = "insert into kelas(nama_kelas,prodi,fakultas) values ('$nama_kelas','$prodi','$fakultas')";
            $konek1      = mysqli_query($connect,$sql1);
            if($konek1){
                $suksess    ="Berhasil Memasukan Data baru";
            }else{
                $error      ="Gagal memasukan data";
            }
        }
    }else{
        $error ="Silahkan Masukan semua data yang tertera";
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Data Kelas</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <style>
    .mx-auto {width : 800px}
    .card { margin-top : 10px;}
    </style>

</head>
<body>
    <div class="mx-auto">
        <!-- untuk memasukan data -->
        <div class="card">
            <div class="card-header">
                Create / Edit Data Kelas
            </div>
            <div class="card-body">
                <?php
                if($error){
                ?>
                    <div class="alert alert-danger" role="alert">
                        <?php echo $error ?>
                    </div>
                <?php
                    header("refresh:5;url=datakelas.php.php"); // setelah memasukan data maka akan kembali ke halaman index.php setelah 5 detik
                }
                ?>
                <?php
                if($suksess){
                ?>
                    <div class="alert alert-success" role="alert">
                        <?php echo $suksess ?>
                    </div>
                <?php
                    header("refresh:5;url=datakelas.php");
                }
                ?>
                <form action="" method="POST">
                    <div class="mb-3 row">
                        <label for="nama_kelas" class="col-sm-2 col-form-label">Nama Kelas</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="nama_kelas" name="nama_kelas" value="<?php echo $nama_kelas?>">
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label for="prodi" class="col-sm-2 col-form-label">Prodi</label>
                        <div class="col-sm-10">
                        <select class="form-control" name="prodi" id="prodi">
                                <option value=""> Pilih Prodi</option>
                                <option value="Sistem Informasi" <?php if($prodi == "Sistem Informasi") echo "selected"?> >Sistem Informasi </option>
                                <option value="Pendidikan Teknik Informatika"<?php if($fakultas == "Pendidikan Teknik Informatika") echo "selected"?> >Pendidikan Teknik Informatika</option>
                            </select>
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label for="fakultas" class="col-sm-2 col-form-label">Fakultas</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="fakultas" id="fakultas">
                                <option value=""> Pilih Fakutlas</option>
                                <option value="Fakultas Ekonomi" <?php if($fakultas == "Fakultas Ekonomi") echo "selected"?> >Fakultas Ekonomi (FE)</option>
                                <option value="Fakultas Teknik Dan Kejuruan"<?php if($fakultas == "Fakultas Teknik Dan Kejuruan") echo "selected"?> >Fakultas Teknik Dan Kejuruan (FTK)</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-12">
                        <input type="submit" name="simpan" value="Simpan data" class="btn btn-primary">
                    </div>
                </form>
            </div>
        </div>

        <!-- untuk mengeluarkan data -->
        <div class="card">
            <div class="card-header text-white bg-secondary">
                Data Kelas
            </div>
            <div class="card-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th scop="col">#</th>
                            <th scop="col">Nama Kelas</th>
                            <th scop="col">Prodi</th>
                            <th scop="col">Fakultas</th>
                            <th scop="col">Action</th>
                        </tr>
                        <tbody>
                            <?php
                                $sql2       ="SELECT * FROM kelas ORDER BY id_kelas DESC";
                                $konek2     = mysqli_query($connect,$sql2);
                                $no         = 1;
                                while($r2   = mysqli_fetch_array($konek2)){
                                    $id             = $r2['id_kelas'];
                                    $nama_kelas     = $r2['nama_kelas'];
                                    $prodi          = $r2['prodi'];
                                    $fakultas       = $r2['fakultas'];   
                                    ?>
                                    <tr>
                                        <th scope="row"><?php echo $no++ ?></th>
                                        <td scope="row"><?php echo $nama_kelas ?></td>
                                        <td scope="row"><?php echo $prodi ?></td>
                                        <td scope="row"><?php echo $fakultas ?></td>
                                        <td scope="row">
                                            <a href="datakelas.php?op=edit&id_kelas=<?php echo $id?>"><button type="button" class="btn btn-warning">Edit</button></a>
                                            <a href="datakelas.php?op=delete&id_kelas=<?php echo $id?>" onclick="return confirm('Yakin Ingin Menghapus Data ?')"><button type="button" class="btn btn-danger">Delete</button></a>           
                                        </td>
                                    </tr>
                                    <?php
                                }
                            ?>
                        </tbody>
                    </thead>
                </table>
            </div>
        </div>
        <a href="index.php">Data Dosen</a><br>
        <a href="datajadwalkelas.php">Data Jadwal Kelas</a>
    </div>
</body>
</html>