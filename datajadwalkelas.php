<?php

include_once("koneksi.php");

$id_dosen       ="";
$id_kelas       ="";
$jadwal         ="";
$mata_kuliah    ="";
$suksess        ="";
$error          ="";

if(isset($_GET['op'])){
    $op = $_GET['op'];
}else{
    $op ="";
}
if($op == 'delete'){
    $id         = $_GET['id_jadwal'];
    $sql1       = "DELETE FROM jadwal WHERE id_jadwal = '$id'";
    $konek1     = mysqli_query($connect,$sql1);
    if($konek1){
        $suksess = "Berhasil hapus data";
    }else{
        $error  = "Gagal melakukan delete data";
    }
}
if($op == 'edit'){
    $id             = $_GET['id_jadwal'];
    $sql1           = "SELECT * FROM jadwal WHERE id_jadwal = '$id'";
    $konek1         = mysqli_query($connect,$sql1);
    $r1             = mysqli_fetch_array($konek1);
    $id_dosen       = $r1['id_dosen'];
    $id_kelas       = $r1['id_kelas'];
    $jadwal         = $r1['jadwal'];
    $mata_kuliah     = $r1['mata_kuliah'];
    
    if($mata_kuliah == ''){
        $error  = "Data tidak ada";
    }
}
if(isset($_POST['simpan'])){ // untuk create data
    $id_dosen       =$_POST['id_dosen'];
    $id_kelas       =$_POST['id_kelas'];
    $jadwal         =$_POST['jadwal'];
    $mata_kuliah     =$_POST['mata_kuliah'];

    if($id_dosen && $id_kelas && $jadwal && $mata_kuliah){
        if($op == 'edit'){ // untuk update
            $sql1       ="update jadwal set id_dosen = '$id_dosen',id_kelas = '$id_kelas',jadwal= '$jadwal',mata_kuliah = '$mata_kuliah' where id_jadwal = '$id'";
            $konek1     = mysqli_query($connect,$sql1);
            if($konek1){
                $suksess = "Data berhasil di Update";
            }else{
                $error   = "Data gagal di Update";
            }
        }else{ // untuk insert
            $sql1        = "insert into jadwal(id_dosen,id_kelas,jadwal,mata_kuliah) values ('$id_dosen','$id_kelas','$jadwal','$mata_kuliah')";
            $konek1      = mysqli_query($connect,$sql1);
            if($konek1){
                $suksess    ="Berhasil Memasukan Data baru";
            }else{
                $error      ="Gagal memasukan data";
            }
        }
    }else{
        $error ="Silahkan Masukan semua data yang tertera";
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Data Jadwal Kelas</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <style>
    .mx-auto {width : 800px}
    .card { margin-top : 10px;}
    </style>

</head>
<body>
    <div class="mx-auto">
        <!-- untuk memasukan data -->
        <div class="card">
            <div class="card-header">
                Create / Edit Data
            </div>
            <div class="card-body">
                <?php
                if($error){
                ?>
                    <div class="alert alert-danger" role="alert">
                        <?php echo $error ?>
                    </div>
                <?php
                    header("refresh:5;url=datajadwalkelas.php"); // setelah memasukan data maka akan kembali ke halaman index.php setelah 5 detik
                }
                ?>
                <?php
                if($suksess){
                ?>
                    <div class="alert alert-success" role="alert">
                        <?php echo $suksess ?>
                    </div>
                <?php
                    header("refresh:5;url=datajadwalkelas.php");
                }
                ?>
                <form action="" method="POST">
                    <div class="mb-3 row">
                        <label for="id_dosen" class="col-sm-2 col-form-label">Id Dosen</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="id_dosen" name="id_dosen" value="<?php echo $id_dosen ?>">
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label for="id_kelas" class="col-sm-2 col-form-label">Id Kelas</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="id_kelas" name="id_kelas" value="<?php echo $id_kelas ?>">
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label for="jadwal" class="col-sm-2 col-form-label">Jadwal</label>
                        <div class="col-sm-10">
                            <input type="date" class="form-control" id="jadwal" name="jadwal" value="<?php echo $jadwal ?>">
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label for="mata_kuliah" class="col-sm-2 col-form-label">Mata Kuliah</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="mata_kuliah" name="mata_kuliah" value="<?php echo $mata_kuliah ?>">
                        </div>
                    </div>
                    <div class="col-12">
                        <input type="submit" name="simpan" value="Simpan data" class="btn btn-primary">
                    </div>
                </form>
            </div>
        </div>

        <!-- untuk mengeluarkan data -->
        <div class="card">
            <div class="card-header text-white bg-secondary">
                Data Jadwal Kelas
            </div>
            <div class="card-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th scop="col">#</th>
                            <th scop="col">Id Dosen</th>
                            <th scop="col">Id Kelas</th>
                            <th scop="col">Jadwal</th>
                            <th scop="col">Mata Kuliah</th>
                            <th scop="col">Action</th>
                        </tr>
                        <tbody>
                            <?php
                                $sql2       ="SELECT * FROM jadwal ORDER BY id_jadwal DESC";
                                $konek2     = mysqli_query($connect,$sql2);
                                $no         = 1;
                                while($r2   = mysqli_fetch_array($konek2)){
                                    $id             = $r2['id_jadwal'];
                                    $id_dosen       = $r2['id_dosen'];
                                    $id_kelas       = $r2['id_kelas'];
                                    $jadwal         = $r2['jadwal'];
                                    $mata_kuliah    = $r2['mata_kuliah']; 
                                    
                                    ?>
                                    <tr>
                                        <th scope="row"><?php echo $no++ ?></th>
                                        <td scope="row"><?php echo $id_dosen ?></td>
                                        <td scope="row"><?php echo $id_kelas ?></td>
                                        <td scope="row"><?php echo $jadwal ?></td>
                                        <td scope="row"><?php echo $mata_kuliah ?></td>
                                        <td scope="row">
                                            <a href="datajadwalkelas.php?op=edit&id_jadwal=<?php echo $id?>"><button type="button" class="btn btn-warning">Edit</button></a>
                                            <a href="datajadwalkelas.php?op=delete&id_jadwal=<?php echo $id?>" onclick="return confirm('Yakin Ingin Menghapus Data ?')"><button type="button" class="btn btn-danger">Delete</button></a>           
                                        </td>
                                    </tr>
                                    <?php
                                }
                            ?>
                        </tbody>
                    </thead>
                </table>
            </div>
        </div>
        <a href="datakelas.php">Data Kelas</a><br>
        <a href="index.php">Data Dosen</a>
    </div>
</body>
</html>