<?php

include_once("koneksi.php");

$foto_dosen     ="";
$nip_dosen      ="";
$nama_dosen     ="";
$prodi          ="";
$fakultas       ="";
$suksess        ="";
$error          ="";

if(isset($_GET['op'])){
    $op = $_GET['op'];
}else{
    $op ="";
}
if($op == 'delete'){
    $id         = $_GET['id_dosen'];
    $sql1       = "DELETE FROM dosen WHERE id_dosen = '$id'";
    $konek1     = mysqli_query($connect,$sql1);
    if($konek1){
        $suksess = "Berhasil hapus data";
    }else{
        $error  = "Gagal melakukan delete data";
    }
}
if($op == 'edit'){
    $id             = $_GET['id_dosen'];
    $sql1           = "SELECT * FROM dosen WHERE id_dosen = '$id'";
    $konek1         = mysqli_query($connect,$sql1);
    $r1             = mysqli_fetch_array($konek1);
    $foto_dosen     = $r1['foto_dosen'];
    $nip_dosen      = $r1['nip_dosen'];
    $nama_dosen     = $r1['nama_dosen'];
    $prodi          = $r1['prodi'];
    $fakultas       = $r1['fakultas'];
    
    if($nip_dosen == ''){
        $error  = "Data tidak ada";
    }
}
if(isset($_POST['simpan'])){ // untuk create data
    $foto_dosen     =$_POST['foto_dosen'];
    $nip_dosen      =$_POST['nip_dosen'];
    $nama_dosen     =$_POST['nama_dosen'];
    $prodi          =$_POST['prodi'];
    $fakultas       =$_POST['fakultas'];

    if($foto_dosen && $nip_dosen && $nama_dosen && $prodi && $fakultas){
        if($op == 'edit'){ // untuk update
            $sql1       ="update dosen set foto_dosen = '$foto_dosen',nip_dosen = '$nip_dosen',nama_dosen= '$nama_dosen',prodi = '$prodi',fakultas = '$fakultas' where id_dosen = '$id'";
            $konek1     = mysqli_query($connect,$sql1);
            if($konek1){
                $suksess = "Data berhasil di Update";
            }else{
                $error   = "Data gagal di Update";
            }
        }else{ // untuk insert
            $sql1        = "insert into dosen(foto_dosen,nip_dosen,nama_dosen,prodi,fakultas) values ('$foto_dosen','$nip_dosen','$nama_dosen','$prodi','$fakultas')";
            $konek1      = mysqli_query($connect,$sql1);
            if($konek1){
                $suksess    ="Berhasil Memasukan Data baru";
            }else{
                $error      ="Gagal memasukan data";
            }
        }
    }else{
        $error ="Silahkan Masukan semua data yang tertera";
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Data Dosen</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <style>
    .mx-auto {width : 800px}
    .card { margin-top : 10px;}
    </style>

</head>
<body>
    <div class="mx-auto">
        <!-- untuk memasukan data -->
        <div class="card">
            <div class="card-header">
                Create / Edit Data
            </div>
            <div class="card-body">
                <?php
                if($error){
                ?>
                    <div class="alert alert-danger" role="alert">
                        <?php echo $error ?>
                    </div>
                <?php
                    header("refresh:5;url=index.php"); // setelah memasukan data maka akan kembali ke halaman index.php setelah 5 detik
                }
                ?>
                <?php
                if($suksess){
                ?>
                    <div class="alert alert-success" role="alert">
                        <?php echo $suksess ?>
                    </div>
                <?php
                    header("refresh:5;url=index.php");
                }
                ?>
                <form action="" method="POST">
                    <div class="mb-3 row">
                        <label for="foto_dosen" class="col-sm-2 col-form-label">Foto Dosen</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="foto_dosen" name="foto_dosen" value="<?php echo $foto_dosen ?>">
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label for="nip_dosen" class="col-sm-2 col-form-label">Nip Dosen</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="nip_dosen" name="nip_dosen" value="<?php echo $nip_dosen ?>">
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label for="nama_dosen" class="col-sm-2 col-form-label">Nama Dosen</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="nama_dosen" name="nama_dosen" value="<?php echo $nama_dosen ?>">
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label for="prodi" class="col-sm-2 col-form-label">Prodi</label>
                        <div class="col-sm-10">
                        <select class="form-control" name="prodi" id="prodi">
                                <option value=""> Pilih Prodi</option>
                                <option value="Sistem Informasi" <?php if($prodi == "Sistem Informasi") echo "selected"?> >Sistem Informasi </option>
                                <option value="Pendidikan Teknik Informatika"<?php if($fakultas == "Pendidikan Teknik Informatika") echo "selected"?> >Pendidikan Teknik Informatika</option>
                            </select>
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label for="fakultas" class="col-sm-2 col-form-label">Fakultas</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="fakultas" id="fakultas">
                                <option value=""> Pilih Fakutlas</option>
                                <option value="Fakultas Ekonomi" <?php if($fakultas == "Fakultas Ekonomi") echo "selected"?> >Fakultas Ekonomi (FE)</option>
                                <option value="Fakultas Teknik Dan Kejuruan"<?php if($fakultas == "Fakultas Teknik Dan Kejuruan") echo "selected"?> >Fakultas Teknik Dan Kejuruan (FTK)</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-12">
                        <input type="submit" name="simpan" value="Simpan data" class="btn btn-primary">
                    </div>
                </form>
            </div>
        </div>

        <!-- untuk mengeluarkan data -->
        <div class="card">
            <div class="card-header text-white bg-secondary">
                Data Dosen
            </div>
            <div class="card-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th scop="col">#</th>
                            <th scop="col">Foto Dosen</th>
                            <th scop="col">Nip Dosen</th>
                            <th scop="col">Nama Dosen</th>
                            <th scop="col">Prodi</th>
                            <th scop="col">Fakultas</th>
                            <th scop="col">Action</th>
                        </tr>
                        <tbody>
                            <?php
                                $sql2       ="SELECT * FROM dosen ORDER BY id_dosen DESC";
                                $konek2     = mysqli_query($connect,$sql2);
                                $no         = 1;
                                while($r2   = mysqli_fetch_array($konek2)){
                                    $id             = $r2['id_dosen'];
                                    $foto_dosen     = $r2['foto_dosen'];
                                    $nip_dosen      = $r2['nip_dosen'];
                                    $nama_dosen     = $r2['nama_dosen'];
                                    $prodi          = $r2['prodi'];
                                    $fakultas       = $r2['fakultas'];   
                                    
                                    ?>
                                    <tr>
                                        <th scope="row"><?php echo $no++ ?></th>
                                        <td scope="row"><?php echo $foto_dosen ?></td>
                                        <td scope="row"><?php echo $nip_dosen ?></td>
                                        <td scope="row"><?php echo $nama_dosen ?></td>
                                        <td scope="row"><?php echo $prodi ?></td>
                                        <td scope="row"><?php echo $fakultas ?></td>
                                        <td scope="row">
                                            <a href="index.php?op=edit&id_dosen=<?php echo $id?>"><button type="button" class="btn btn-warning">Edit</button></a>
                                            <a href="index.php?op=delete&id_dosen=<?php echo $id?>" onclick="return confirm('Yakin Ingin Menghapus Data ?')"><button type="button" class="btn btn-danger">Delete</button></a>           
                                        </td>
                                    </tr>
                                    <?php
                                }
                            ?>
                        </tbody>
                    </thead>
                </table>
            </div>
        </div>
        <a href="datakelas.php">Data Kelas</a><br>
        <a href="datajadwalkelas.php">Data Jadwal Kelas</a>
    </div>
</body>
</html>